/*****************************************************************************************************************************
 * @file        Adc.c                                                                                                        *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        21.03.2019 06:32:10                                                                                          *
 * @brief       Generated Rte Module Interface file: Adc.c                                                                   *
 * @version     1.0.0                                                                                                        *
 * @generator   OSAR Rte Module Generator v.0.1.7.10                                                                         *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.6.2                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Adc 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Rte_Types.h"
#include "Rte_Adc.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of USER include and definition area           << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "Adc.h"

/*---------------------------------------------------- External variables --------------------------------------------------*/
extern ADC_HandleTypeDef adcHdlObjs[ADC_CNT_HDL_OBJS];
extern Adc_ChannelCfgType adcChannelObjs[ADC_CNT_CHANNEL_OBJS];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           End of USER include and definition area            << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map functions into code memory ---------------------------------------------*/
#define Adc_START_SEC_CODE
#include "Adc_MemMap.h"
/**
 * @brief           Rte generated module initialization function.
 * @param[in]       None
 * @retval          None
 * @note            Trigger: System Init
 * @uuid            c606989127d04f6b963895f8a044e705
 */
VAR(void) Adc_Init( VAR(void) )
{
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>             Start of user code implementation              << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
  /* Call Hardware initialization */
  Adc_HwInit();
  /**************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>              End of user code implementation               << DO NOT CHANGE THIS COMMENT */
  /**************************************************************************************************************************/
}

#define Adc_STOP_SEC_CODE
#include "Adc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of USER function area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define Adc_START_SEC_CODE
#include "Adc_MemMap.h"
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void Adc_InitMemory( void )
{

}

/* ######################################################################################################################## */
/* ######################################### ADC PROVIDED HIGH LEVEL INTERFACES ########################################### */
/* ######################################################################################################################## */
/**
 * @brief           API function to request an sampled voltage from an configured ADC-Channel.
 * @param[in]       uint16 Channel ID where the voltage shall be sampled.
 * @param[out]      uint32 Pointer to write the result data to
 * @details         API requests the channel voltage sychrounous. This results in a blocking the CPU during the request.
 * @retval          Adc_ReturnType
 *                  > ADC_E_OK
 *                  > ADC_E_NOT_OK
 */
Adc_ReturnType Adc_GetSyncChannelVoltage(uint16 channelId, uint32* pOutData)
{
  Adc_ReturnType retVal = ADC_E_NOT_OK;
  HAL_StatusTypeDef halRetVal;
  uint16 adcConfigId, idx;
  boolean configFound = FALSE;

  /* Check input parameter */
if(NULL_PTR == pOutData)
  {
#if (ADC_MODULE_USE_DET == STD_ON)
    Det_ReportError(ADC_DET_MODULE_ID, ADC_E_NULL_POINTER);
#endif
    retVal = ADC_E_NOT_OK;
  }
  else
  {
    /* Search for correct ADC Hanlder */
    for(idx = 0; idx < ADC_CNT_CHANNEL_OBJS; idx++)
    {
        if(adcChannelObjs[idx].channelCfg.Channel == channelId)
        {
          adcConfigId = idx;
          configFound = TRUE;
        }
    }

    if(FALSE == configFound)
    {
#if (ADC_MODULE_USE_DET == STD_ON)
      Det_ReportError(ADC_DET_MODULE_ID, ADC_E_INVALID_CONFIGURATION);
#endif
      retVal = ADC_E_NOT_OK;
    }
    else
    {
      /* Reconfigure ADC channel */
      halRetVal = HAL_ADC_ConfigChannel(adcChannelObjs[adcConfigId].moduleHdl, &adcChannelObjs[adcConfigId].channelCfg);
      if(HAL_OK != halRetVal)
      {
#if (ADC_MODULE_USE_DET == STD_ON)
      Det_ReportError(ADC_DET_MODULE_ID, ADC_E_HAL_ERROR);
#endif
      retVal = ADC_E_NOT_OK;
      }
      else
      {
        /* Start ADC channel conversion */
        halRetVal = HAL_ADC_Start(adcChannelObjs[adcConfigId].moduleHdl);
        if(HAL_OK != halRetVal)
        {
  #if (ADC_MODULE_USE_DET == STD_ON)
          Det_ReportError(ADC_DET_MODULE_ID, ADC_E_HAL_ERROR);
  #endif
          retVal = ADC_E_NOT_OK;
        }
        else
        {
          /* Poll for conversion has finished */
          halRetVal = HAL_ADC_PollForConversion(adcChannelObjs[adcConfigId].moduleHdl, ADC_CONVERSION_TIMEOUT_MS);
          if(HAL_OK != halRetVal)
          {
    #if (ADC_MODULE_USE_DET == STD_ON)
            Det_ReportError(ADC_DET_MODULE_ID, ADC_E_HAL_ERROR);
    #endif
            retVal = ADC_E_NOT_OK;
          }
          else
          {
            /* Get ADC Value */
            *pOutData = HAL_ADC_GetValue(adcChannelObjs[adcConfigId].moduleHdl);
            retVal = ADC_E_OK;
          }
        }
      }
    }
  }

  return retVal;
}


#define Adc_STOP_SEC_CODE
#include "Adc_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of USER function area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

