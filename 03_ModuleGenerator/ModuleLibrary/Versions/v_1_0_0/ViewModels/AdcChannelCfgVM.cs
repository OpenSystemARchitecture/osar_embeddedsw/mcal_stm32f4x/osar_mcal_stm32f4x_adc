﻿/*****************************************************************************************************************************
 * @file        AdcModuleCfgVM.cs                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        31.12.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Adc module configuration                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using ModuleLibrary.Versions.v_1_0_0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /****************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
  /****************************************************************************************************************************/
  public class AdcChannelCfgVM : BaseViewModel
  {
    private AdcChannelCfg usedAdcChannel;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="adcChannel"></param>
    /// <param name="storeCfgDel"></param>
    public AdcChannelCfgVM(AdcChannelCfg adcChannel, StoreConfiguration storeCfgDel) : base(storeCfgDel)
    {
      usedAdcChannel = adcChannel;
    }

    /// <summary>
    /// Interface for Attribute object usedAdcChannel
    /// </summary>
    public AdcChannelCfg AdcChannel { get => usedAdcChannel; set => usedAdcChannel = value; }

    /// <summary>
    /// Interface for Attribute object adcChannelName
    /// </summary>
    public String AdcChannelName
    {
      get => usedAdcChannel.adcChannelName;
      set => usedAdcChannel.adcChannelName = value;
    }

    /// <summary>
    /// Interface for Attribute object adcUsedAdcModuleCfgName
    /// </summary>
    public String AdcUsedAdcModuleCfgName
    {
      get => usedAdcChannel.adcUsedAdcModuleCfgName;
      set => usedAdcChannel.adcUsedAdcModuleCfgName = value;
    }

    /// <summary>
    /// Interface for Attribute object adcUsedAdcChannelCfgIdx
    /// </summary>
    public Byte AdcUsedAdcChannelCfgIdx
    {
      get => usedAdcChannel.adcUsedAdcChannelCfgIdx;
      set => usedAdcChannel.adcUsedAdcChannelCfgIdx = value;
    }

    /// <summary>
    /// Interface for Attribute object adcUsedSamplingTime
    /// </summary>
    public AdcChannelSamplingTime AdcUsedSamplingTime
    {
      get => usedAdcChannel.adcUsedSamplingTime;
      set => usedAdcChannel.adcUsedSamplingTime = value;
    }
  }
}
/**
 * @}
 */
