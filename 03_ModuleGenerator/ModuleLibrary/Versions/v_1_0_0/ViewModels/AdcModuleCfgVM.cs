﻿/*****************************************************************************************************************************
 * @file        AdcModuleCfgVM.cs                                                                                            *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        31.12.2019                                                                                                   *
 * @brief       Implementation of the View Model from the Adc module configuration                                           *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using ModuleLibrary.Versions.v_1_0_0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /****************************************************************************************************************************/
  /* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
  /****************************************************************************************************************************/
  public class AdcModuleCfgVM : BaseViewModel
  {
    private AdcModuleCfg usedAdcModule;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="baseModule"></param>
    /// <param name="storeCfgDel"></param>
    public AdcModuleCfgVM(AdcModuleCfg adcModule, StoreConfiguration storeCfgDel) : base(storeCfgDel)
    {
      usedAdcModule = adcModule;
    }

    /// <summary>
    /// Interface for Attribute object blockCfgData
    /// </summary>
    public AdcModuleCfg AdcModulConfig { get => usedAdcModule; set => usedAdcModule = value; }

    /// <summary>
    /// Interface for Attribute object adcModuleCfgName
    /// </summary>
    public String AdcModuleCfgName
    { 
      get => usedAdcModule.adcModuleCfgName; 
      set => usedAdcModule.adcModuleCfgName = value; 
    }

    /// <summary>
    /// Interface for Attribute object adcUsedAdcModule
    /// </summary>
    public AdcModule AdcUsedAdcModule
    {
      get => usedAdcModule.adcUsedAdcModule;
      set => usedAdcModule.adcUsedAdcModule = value;
    }

    /// <summary>
    /// Interface for Attribute object adcUsedClockPrescaler
    /// </summary>
    public AdcModuleClockPrescaler AdcUsedClockPrescaler
    {
      get => usedAdcModule.adcUsedClockPrescaler;
      set => usedAdcModule.adcUsedClockPrescaler = value;
    }

    /// <summary>
    /// Interface for Attribute object adcUsedResolution
    /// </summary>
    public AdcModuleConversionResolution AdcUsedResolution
    {
      get => usedAdcModule.adcUsedResolution;
      set => usedAdcModule.adcUsedResolution = value;
    }

    /// <summary>
    /// Interface for Attribute object adcUsedDataAlignment
    /// </summary>
    public AdcModuleConversionAlignment AdcUsedDataAlignment
    {
      get => usedAdcModule.adcUsedDataAlignment;
      set => usedAdcModule.adcUsedDataAlignment = value;
    }

  }
}
/**
 * @}
 */
