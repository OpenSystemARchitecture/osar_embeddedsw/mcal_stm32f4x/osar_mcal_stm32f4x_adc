﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModuleLibrary.Versions.v_1_0_0.ViewModels;

namespace ModuleLibrary.Versions.v_1_0_0.Views
{
  /// <summary>
  /// Interaction logic for the ModuleCfgView.xaml
  /// </summary>
  public partial class ModuleCfgView : UserControl
  {
    ViewModels.Module_ViewModel newCfg;
    string pathToConfigFile;
    string absPathModuleBaseFolder;

  /// <summary>
  /// Constructor
  /// </summary>
  /// <param name="pathToCfgFile">Absolute Path to configuration file</param>
  /// <param name="absPathToModuleBaseFolder">Absolute Path to module base folder</param>
  public ModuleCfgView(string pathToCfgFile, string absPathToModuleBaseFolder)
    {
      /* Check if config file path is an rooted one */
      if (true == System.IO.Path.IsPathRooted(pathToCfgFile))
      {
        pathToConfigFile = pathToCfgFile;
      }
      else
      {
        pathToConfigFile = absPathToModuleBaseFolder + pathToCfgFile;
      }

      newCfg = new ViewModels.Module_ViewModel(pathToCfgFile, absPathToModuleBaseFolder);

      InitializeComponent();

      /* Set Data Context where data is binded to*/
      this.DataContext = newCfg;
    }

    /// <summary>
    /// Button to add a new Adc module
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewAdcModule_Click(object sender, RoutedEventArgs e)
    {
      newCfg.AddNewAdcModule();
    }

    /// <summary>
    /// Interface to remove the selected ADC module
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_RemoveSelectedAdcModule_Click(object sender, RoutedEventArgs e)
    {
      AdcModuleCfgVM removedAdc = (AdcModuleCfgVM)DG_AdcModuleList.SelectedItem;
      newCfg.RemoveAdcModule(removedAdc);
    }

    /// <summary>
    /// Interface to add a new adc channel
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewAdcChannel_Click(object sender, RoutedEventArgs e)
    {
      newCfg.AddNewAdcChannel();
    }

    private void B_RemoveSelectedAdcChannel_Click(object sender, RoutedEventArgs e)
    {
      AdcChannelCfgVM removedAdcChannel = (AdcChannelCfgVM)DG_AdcChannelList.SelectedItem;
      newCfg.RemoveAdcChannel(removedAdcChannel);
    }
  }
}
