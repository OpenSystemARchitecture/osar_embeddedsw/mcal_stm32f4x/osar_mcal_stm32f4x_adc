﻿/*****************************************************************************************************************************
 * @file        ModuleGenerator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Generator Class                                                                 *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.Generator.Resources;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using OsarSourceFileLib.DocumentationObjects.Doxygen;
using OsarSourceFileLib.CFile;
using OsarSourceFileLib.CFile.CFileObjects;
using OsarSourceFileLib.DocumentationObjects.General;
using RteLib;
using ModuleLibrary.Versions.v_1_0_0.Models;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleGenerator
  {
    private Models.AdcXml xmlCfg;
    private string pathToConfiguratioFile;
    private string pathToModuleBaseFolder;
    private string moduleLibPath;
    private GenInfoType info;

    


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    /// <param name="absPathToBaseModuleFolder"> Absolute path to module base folder</param>
    public ModuleGenerator(Models.AdcXml cfgFile, string pathToCfgFile, string absPathToBaseModuleFolder)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;
      pathToModuleBaseFolder = absPathToBaseModuleFolder;

      moduleLibPath = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to generate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType GenerateConfiguration()
    {
      // Generate Memory-Map-Header-File
      GenerateMemoryMapHeaderFile();

      // Generate C-Source-File
      GenerateConfigurationSourceFile();

      // Generate C-Header-File
      GenerateConfigurationHeaderFile();

      // Generate Rte-Interface-XML-File
      GenerateConfigurationRteInterfaceXmlFile();

      return info;
    }

    /// <summary>
    /// Interface to generate the Memory-Map-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateMemoryMapHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_StartGenMemMap);

      /* +++++ Generate Memory Mapping file +++++ */
      OsarResources.Generic.OsarGenericHelper.generateModuleMemoryMappingFile(pathToModuleBaseFolder + GenResources.GenHeaderFilePath, DefResources.ModuleName, DefResources.GeneratorName);

      info.AddLogMsg(GenResources.LogMsg_MemMapGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Source-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationSourceFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenSourceCfgFile);

      CSourceFile modulePBCfgSource = new CSourceFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects sourceIncludes = new CFileIncludeObjects();
      CFileVariableObjects sourceVariables = new CFileVariableObjects();
      CFileFunctionObjects sourceFunction = new CFileFunctionObjects();

      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      int actualGroupIdx;
      string tempString = "", tempString2 = "";

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Implementation of Adc module *_PBCfg.c file");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupMCAL_STM32F4Cube);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      sourceIncludes.AddIncludeFile(DefResources.ModuleName + "_PBCfg.h");
      sourceIncludes.AddIncludeFile("Det.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare Variables ++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create Module Configuration */
      generalStartGroup.AddGroupName("Map variables into uninitialized memory");
      generalStartGroup.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_NOINIT_VAR);
      generalEndGroup.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_NOINIT_VAR);
      actualGroupIdx = sourceVariables.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      sourceVariables.AddVariable("ADC_HandleTypeDef", "adcHdlObjs[ADC_CNT_HDL_OBJS]", actualGroupIdx, "", "");

      /* Create Channel Configuration */
      generalStartGroup.AddGroupName("Map variables into initialized memory");
      generalStartGroup.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_INIT_VAR);
      generalEndGroup.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_INIT_VAR);
      actualGroupIdx = sourceVariables.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      tempString = Hlp_CreatChannelArrayInitData();
      sourceVariables.AddArray("Adc_ChannelCfgType", "adcChannelObjs", "ADC_CNT_CHANNEL_OBJS", actualGroupIdx, tempString, "", arrayRearrangeType.STD_ARRAY);



      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Source Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create Hw Init function */
      generalStartGroup.AddGroupName("Map code into code memory");
      generalStartGroup.AddOsarStartMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
      generalEndGroup.AddOsarStopMemMapDefine(DefResources.ModuleName, OsarMemMapDataType.OSAR_CODE);
      actualGroupIdx = sourceFunction.AddCFileObjectGroup(generalStartGroup, generalEndGroup);

      doxygenDescription.AddElementBrief("Module internal hardware initialization function");
      doxygenDescription.AddElementDetails("This function would be generated with to initialize the configured ADC Hardware modules.");
      doxygenDescription.AddElementReturnValues("None");
      doxygenDescription.AddElementParameter("None", CParameterType.INPUT);
      tempString = Hlp_CreatHwInitFncContent();
      sourceFunction.AddFunction("void", "Adc_HwInit", "void", actualGroupIdx, doxygenDescription, tempString);
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgSource.AddFileName(DefResources.ModuleName + "_PBCfg.c");
      modulePBCfgSource.AddFilePath(pathToModuleBaseFolder + GenResources.GenSourceFilePath);
      modulePBCfgSource.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgSource.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgSource.AddIncludeObject(sourceIncludes);
      modulePBCfgSource.AddGlobalVariableObject(sourceVariables);
      modulePBCfgSource.AddGlobalFunctionObject(sourceFunction);
      modulePBCfgSource.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_SourceCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration C-Header-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationHeaderFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenHeaderCfgFile);

      CHeaderFile modulePBCfgHeader = new CHeaderFile();
      DoxygenFileHeader doxygenFileHeader = new DoxygenFileHeader();
      DoxygenFileGroupOpener doxygenFileGroupOpener = new DoxygenFileGroupOpener();
      DoxygenFileGroupCloser doxygenFileGroupCloser = new DoxygenFileGroupCloser();
      CFileDefinitionObjects headerDefinitions = new CFileDefinitionObjects();
      GeneralStartGroupObject generalStartGroup = new GeneralStartGroupObject();
      GeneralEndGroupObject generalEndGroup = new GeneralEndGroupObject();
      CFileIncludeObjects headerIncludes = new CFileIncludeObjects();
      CFileVariableObjects headerVariables = new CFileVariableObjects();
      CFileFunctionObjects headerFunction = new CFileFunctionObjects();
      DoxygenElementDescription doxygenDescription = new DoxygenElementDescription();
      CFileTypeObjects headerTypes = new CFileTypeObjects();
      int actualGroupIdx;


      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Prepare general data sets ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenFileHeader.AddFileBrief("Generated header file data of the Adc module.");
      doxygenFileHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      doxygenFileHeader.AddFileNote(OsarResources.Generator.Resources.genericSource.CommentGeneratorDisclaimer);
      doxygenFileHeader.AddFileAuthor(DefResources.GenFileAuthor);
      doxygenFileHeader.AddFileGenerator(DefResources.GeneratorName + " v." + ModuleLibraryVersionClass.getCurrentVersion());
      doxygenFileHeader.AddFileVersion(DefResources.ModuleVersion);
      doxygenFileGroupOpener.AddToDoxygenMasterGroup(OsarResources.Generator.Resources.genericSource.DoxygenOsarMasterGroupMCAL_STM32F4Cube);
      doxygenFileGroupOpener.AddToDoxygenGroup(DefResources.ModuleName);

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Prepare include strings ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      headerIncludes.AddIncludeFile(DefResources.ModuleName + "_Types.h");

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++++ Prepare data types +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Prepare Definitions +++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* Create Det informations */
      generalStartGroup.AddGroupName("Adc Module Det Information");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      headerDefinitions.AddDefinition(GenResources.GenDefineDetModuleId,
        xmlCfg.detModuleID.ToString(), actualGroupIdx);
      headerDefinitions.AddDefinition(GenResources.GenDefineModuleUseDet,
        xmlCfg.detModuleUsage.ToString(), actualGroupIdx);

      /* Create Module configuration informations */
      generalStartGroup.AddGroupName("ADC Module configuration parameters");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      headerDefinitions.AddDefinition(GenResources.GenAdcModuleCfgDefineCntAdcModuleCfg, xmlCfg.adcModuleCfgList.Count().ToString(), actualGroupIdx);
      headerDefinitions.AddDefinition(GenResources.GenAdcModuleCfgDefineCntAdcChannelCfg, xmlCfg.adcChannelCfgList.Count().ToString(), actualGroupIdx);
      headerDefinitions.AddDefinition(GenResources.GenAdcModuleCfgDefineAdcSyncTimeout, xmlCfg.adcSyncRequestTimeout.ToString(), actualGroupIdx);

      /* Create User Module Channel configuration informations */
      generalStartGroup.AddGroupName("User Channel Configuration");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      for (int idx = 0; idx < xmlCfg.adcChannelCfgList.Count(); idx++)
      {
        headerDefinitions.AddDefinition(GenResources.GenAdcModuleCfgDefineAdcChannelNamePrefix + xmlCfg.adcChannelCfgList[idx].adcChannelName.ToUpper(), GenResources.GenHalAdcChannelDefine + xmlCfg.adcChannelCfgList[idx].adcUsedAdcChannelCfgIdx.ToString(), actualGroupIdx);
      }

      /* Create User Module Channel function informations */
      generalStartGroup.AddGroupName("User Channel API");
      actualGroupIdx = headerDefinitions.AddCFileObjectGroup(generalStartGroup, generalEndGroup);
      for (int idx = 0; idx < xmlCfg.adcChannelCfgList.Count(); idx++)
      {
        headerDefinitions.AddDefinition(GenResources.GenAdcModuleCfgDefineAdcGetSyncVolategeFncPrefix + xmlCfg.adcChannelCfgList[idx].adcChannelName + "(x)", "Adc_GetSyncChannelVoltage(" + GenResources.GenHalAdcChannelDefine + xmlCfg.adcChannelCfgList[idx].adcUsedAdcChannelCfgIdx.ToString() + ", x)", actualGroupIdx);
      }
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++ Prepare Header Functions ++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      doxygenDescription.AddElementBrief("Module internal hardware initialization function");
      doxygenDescription.AddElementDetails("This function would be generated with to initialize the configured ADC Hardware modules.");
      doxygenDescription.AddElementReturnValues("None");
      doxygenDescription.AddElementParameter("None", CParameterType.INPUT);
      headerFunction.AddFunction("void", "Adc_HwInit", "void", doxygenDescription);
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      modulePBCfgHeader.AddFileName(DefResources.ModuleName + "_PBCfg.h");
      modulePBCfgHeader.AddFilePath(pathToModuleBaseFolder + GenResources.GenHeaderFilePath);
      modulePBCfgHeader.AddFileCommentHeader(doxygenFileHeader);
      modulePBCfgHeader.AddFileGroup(doxygenFileGroupOpener, doxygenFileGroupCloser);
      modulePBCfgHeader.AddDefinitionObject(headerDefinitions);
      modulePBCfgHeader.AddIncludeObject(headerIncludes);
      modulePBCfgHeader.AddGlobalVariableObject(headerVariables);
      modulePBCfgHeader.AddGlobalFunctionPrototypeObject(headerFunction);
      modulePBCfgHeader.AddTypesObject(headerTypes);
      modulePBCfgHeader.GenerateSourceFile();

      info.AddLogMsg(GenResources.LogMsg_HeaderCfgFileGenerated);
    }

    /// <summary>
    /// Interface to generate the configuration Rte-Interface-XML-File
    /// </summary>
    /// <returns> Generation information </returns>
    public void GenerateConfigurationRteInterfaceXmlFile()
    {
      info.AddLogMsg(GenResources.LogMsg_GenRteMibCfgFile);
      info.AddLogMsg("Using RteLib version: v." + RteLib.VersionClass.major.ToString() + "." +
        RteLib.VersionClass.minor.ToString() + "." + RteLib.VersionClass.patch.ToString());

      // Create main instance of internal behavior file
      RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior internalBehavior = new RteLib.RteModuleInternalBehavior.RteModuleInternalBehavior();
      RteLib.RteInterface.RteGeneralInterfaceInfo runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      RteLib.RteModuleInternalBehavior.RteRunnable initRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      RteLib.RteModuleInternalBehavior.RteRunnable rteRunnable = new RteLib.RteModuleInternalBehavior.RteRunnable();
      RteLib.RteModuleInternalBehavior.RteCyclicRunnable mainfunctionRunnable = new RteLib.RteModuleInternalBehavior.RteCyclicRunnable();

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++ Create Types ++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Create Blueprint Interfaces CS +++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Create Blueprint Interfaces SR +++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create CS Port Interfaces ++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create SR Port Interfaces ++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++ Create Init Runnable ++++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      runabbleInfo = new RteLib.RteInterface.RteGeneralInterfaceInfo();
      runabbleInfo.InterfaceName = GenResources.RteMib_InitRunnableName;
      runabbleInfo.UUID = GenResources.RteMib_InitRunnableName_UUID;

      initRunnable.RunnableInfo = runabbleInfo;

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++++ Create Cyclic Runnable +++++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      /* ++++++++++++++++++++++++++++++++++++++++ Build up and generate file +++++++++++++++++++++++++++++++++++++++ */
      /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
      internalBehavior.ModuleName = DefResources.ModuleName;
      internalBehavior.ModuleType = RteLib.RteConfig.RteConfigModuleTypes.MCAL;
      internalBehavior.UUID = GenResources.RteMib_Module_UUID;
      internalBehavior.InitRunnables.Add(initRunnable);

      internalBehavior.SaveActiveRteModuleInternalBehaviorToXml(pathToModuleBaseFolder + GenResources.GenGeneratorFilePath + "\\" + GenResources.RteCfgFileName);

      info.AddLogMsg(GenResources.LogMsg_RteMibCfgFileGenerated);
    }


    /// <summary>
    /// Helper function to create Hw int function
    /// </summary>
    /// <returns>string with function content </returns>
    private string Hlp_CreatHwInitFncContent()
    {
      string fncContent = "", tmpString = "";
      for (int idx = 0; idx < xmlCfg.adcModuleCfgList.Count(); idx++)
      {
        /* Create comment */
        fncContent += "  " + "/* ADC " + ( idx + 1 ).ToString() + " Module config */ \n";

        /* Create Instance */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Instance = " + xmlCfg.adcModuleCfgList[idx].adcUsedAdcModule.ToString() + ";\n";

        /* Create Clock Prescaler */
        switch (xmlCfg.adcModuleCfgList[idx].adcUsedClockPrescaler)
        {
          case AdcModuleClockPrescaler.ADC_DIV_2:
          tmpString = "ADC_CLOCK_SYNC_PCLK_DIV2";
          break;

          case AdcModuleClockPrescaler.ADC_DIV_4:
          tmpString = "ADC_CLOCK_SYNC_PCLK_DIV4";
          break;

          case AdcModuleClockPrescaler.ADC_DIV_6:
          tmpString = "ADC_CLOCK_SYNC_PCLK_DIV6";
          break;

          case AdcModuleClockPrescaler.ADC_DIV_8:
          tmpString = "ADC_CLOCK_SYNC_PCLK_DIV8";
          break;
        }
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.ClockPrescaler = " + tmpString + ";\n";

        /* Create Resolution */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.Resolution = " + xmlCfg.adcModuleCfgList[idx].adcUsedResolution.ToString() + ";\n";

        /* Create  ScanConvMode */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.ScanConvMode = DISABLE;\n";

        /* Create  ContinuousConvMode */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.ContinuousConvMode = DISABLE;\n";

        /* Create  DiscontinuousConvMode */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.DiscontinuousConvMode = DISABLE;\n";

        /* Create  ExternalTrigConvEdge */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;\n";

        /* Create  ExternalTrigConv */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.ExternalTrigConvEdge = ADC_SOFTWARE_START;\n";

        /* Create  DataAlign */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.DataAlign = " + xmlCfg.adcModuleCfgList[idx].adcUsedDataAlignment.ToString() + ";\n";

        /* Create  NbrOfConversion */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.NbrOfConversion = 1;\n";

        /* Create  DMAContinuousRequests */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.DMAContinuousRequests = DISABLE;\n";

        /* Create  EOCSelection */
        fncContent += "  " + "adcHdlObjs[" + idx.ToString() + "].Init.EOCSelection = ADC_EOC_SINGLE_CONV;\n";

        fncContent += "\n";

        /* Check Init */
        fncContent += "  " + "if (HAL_ADC_Init(&adcHdlObjs[" + idx.ToString() + "]) != HAL_OK) \n";
        fncContent += "  " + "{\n";
        fncContent += "#if (ADC_MODULE_USE_DET == STD_ON)\n";
        fncContent += "    " + "Det_ReportError(ADC_DET_MODULE_ID, ADC_E_GENERIC_PROGRAMMING_FAILURE); \n";
        fncContent += "#endif\n";
        fncContent += "  " + "}\n";
        fncContent += "\n";
      }

      return fncContent;
    }

    /// <summary>
    /// Helper function to create channel array init data
    /// </summary>
    /// <returns>string with array init</returns>
    private string Hlp_CreatChannelArrayInitData()
    {
      string arrayInit = "";
      int moduleCfgIdx;

      for (int idx = 0; idx < xmlCfg.adcChannelCfgList.Count(); idx++)
      {
        /* Create init String */
        if (idx == 0)
        { arrayInit += "{"; }
        else
        { arrayInit += ", {"; }


        /* Search for correct adc module */
        for (moduleCfgIdx = 0; moduleCfgIdx < xmlCfg.adcModuleCfgList.Count(); moduleCfgIdx++)
        {
          if (xmlCfg.adcChannelCfgList[idx].adcUsedAdcModuleCfgName == xmlCfg.adcModuleCfgList[moduleCfgIdx].adcModuleCfgName)
          {
            break;
          }
        }

        arrayInit += "&adcHdlObjs[" + moduleCfgIdx.ToString() + "], {" + GenResources.GenHalAdcChannelDefine + xmlCfg.adcChannelCfgList[idx].adcUsedAdcChannelCfgIdx.ToString() +
          ", 1, " + xmlCfg.adcChannelCfgList[idx].adcUsedSamplingTime.ToString() + ", 0} }";

      }

      return arrayInit;
    }
  }
}
/**
 * @}
 */
