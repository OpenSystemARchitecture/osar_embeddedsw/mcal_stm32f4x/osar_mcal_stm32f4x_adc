﻿/*****************************************************************************************************************************
 * @file        ModuleValidator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Validation Class                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.XML;
using OsarResources.Generator.Resources;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleValidator
  {
    private Models.AdcXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public ModuleValidator(Models.AdcXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to validate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType ValidateConfiguration()
    {
      bool moduleCfgFound;
      info.AddLogMsg(ValidateResources.LogMsg_StartOfValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, xmlCfg.xmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }

      /* Check if configuration file parameter is available */
      /* Validate Module Configuration */
      for (int idx = 0; idx < xmlCfg.adcModuleCfgList.Count(); idx++)
      {
        /* Check module config name */
        info.AddLogMsg(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Validate module config name.");
        if (xmlCfg.adcModuleCfgList[idx].adcModuleCfgName == null)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1000), GenErrorWarningCodes.Error_1000_Msg + idx.ToString());
        }
        if (xmlCfg.adcModuleCfgList[idx].adcModuleCfgName.Contains(" "))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg + idx.ToString());
        }
      }


      /* Validate Channel Configuration */
      for (int idx = 0; idx < xmlCfg.adcChannelCfgList.Count(); idx++)
      {
        /* Check channel config name */
        info.AddLogMsg(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Validate adc channel config name. >> Configuration idx = " + idx.ToString());
        if (xmlCfg.adcChannelCfgList[idx].adcChannelName == null)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg + idx.ToString());
        }
        if (xmlCfg.adcChannelCfgList[idx].adcChannelName.Contains(" "))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg + idx.ToString());
        }

        /* Check channel config has a valid module config name */
        info.AddLogMsg(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Validate adc channel module config name. >> Configuration idx = " + idx.ToString());
        if (xmlCfg.adcChannelCfgList[idx].adcUsedAdcModuleCfgName == null)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg + idx.ToString());
        }
        if (xmlCfg.adcChannelCfgList[idx].adcUsedAdcModuleCfgName.Contains(" "))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1005), GenErrorWarningCodes.Error_1005_Msg + idx.ToString());
        }

        /* Search if element dose exist */
        moduleCfgFound = false;
        info.AddLogMsg(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Validate if adc module config name dose exist. >> Configuration idx = " + idx.ToString());
        for (int idx2 = 0; idx2 < xmlCfg.adcModuleCfgList.Count(); idx2++)
        {
          if (xmlCfg.adcChannelCfgList[idx].adcUsedAdcModuleCfgName == xmlCfg.adcModuleCfgList[idx2].adcModuleCfgName)
          {
            info.AddLogMsg(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Adc module configuration found. >> Configuration idx = " + idx.ToString() + " >> Module Configuration idx = " + idx2.ToString());
            moduleCfgFound = true;
            break;
          }
        }
        if (false == moduleCfgFound)
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1006), GenErrorWarningCodes.Error_1006_Msg + idx.ToString());
        }

        /* Check if configured Adc Channel dose exist */
        info.AddLogMsg(OsarResources.Generator.Resources.generatorOutputs.Std_Output + "Validate if adc hardware channel dose exist. >> Configuration idx = " + idx.ToString());
        if (( xmlCfg.adcChannelCfgList[idx].adcUsedAdcChannelCfgIdx < 0 ) || ( xmlCfg.adcChannelCfgList[idx].adcUsedAdcChannelCfgIdx > 18 ))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1007), GenErrorWarningCodes.Error_1007_Msg + idx.ToString());
        }
      }

      info.AddLogMsg(ValidateResources.LogMsg_ValidationDone);
      return info;
    }
  }
}
/**
 * @}
 */
