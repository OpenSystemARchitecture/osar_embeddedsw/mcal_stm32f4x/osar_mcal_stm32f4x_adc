﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;


namespace AdcXsd
{
  /*---------------------------------------------------------------------*/
  /*---------------------------------------------------------------------*/
  /*---------------------------------------------------------------------*/
  public enum AdcModule
  {
    ADC1,
    ADC2,
    ADC3
  }

  public enum AdcModuleClockPrescaler
  {
    ADC_DIV_2,
    ADC_DIV_4,
    ADC_DIV_6,
    ADC_DIV_8
  }

  public enum AdcModuleConversionResolution
  {
    ADC_RESOLUTION_6B,
    ADC_RESOLUTION_8B,
    ADC_RESOLUTION_10B,
    ADC_RESOLUTION_12B
  }

  public enum AdcModuleConversionAlignment
  {
    ADC_DATAALIGN_RIGHT,
    ADC_DATAALIGN_LEFT
  }

  public struct AdcModuleCfg
  {
    public String adcModuleCfgName;
    public AdcModule adcUsedAdcModule;
    public AdcModuleClockPrescaler adcUsedClockPrescaler;
    public AdcModuleConversionResolution adcUsedResolution;
    public AdcModuleConversionAlignment adcUsedDataAlignment;
  }

  /*---------------------------------------------------------------------*/
  /*---------------------------------------------------------------------*/
  /*---------------------------------------------------------------------*/
  public enum AdcChannelSamplingTime
  {
    ADC_SAMPLETIME_3CYCLES,
    ADC_SAMPLETIME_15CYCLES,
    ADC_SAMPLETIME_28CYCLES,
    ADC_SAMPLETIME_56CYCLES,
    ADC_SAMPLETIME_84CYCLES,
    ADC_SAMPLETIME_112CYCLES,
    ADC_SAMPLETIME_144CYCLES,
    ADC_SAMPLETIME_480CYCLES
  }

  public struct AdcChannelCfg
  {
    public String adcChannelName;
    public String adcUsedAdcModuleCfgName;
    public Byte adcUsedAdcChannelCfgIdx;  /* Range 0 - 18 */
    public AdcChannelSamplingTime adcUsedSamplingTime;
  }

  /*---------------------------------------------------------------------*/
  /*---------------------------------------------------------------------*/
  /*---------------------------------------------------------------------*/
  public class AdcXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;

    public AdcModuleCfg[] adcModuleCfgList;
    public AdcChannelCfg[] adcChannelCfgList;

    public UInt16 adcSyncRequestTimeout;
  }
}
