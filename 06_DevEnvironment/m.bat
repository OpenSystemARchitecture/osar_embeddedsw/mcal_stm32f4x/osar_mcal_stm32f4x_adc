echo off
echo Start of Compilation

::Call of makefile 
make -f Makefile %1 -j 4

if %ERRORLEVEL% == 0 (
    echo Done
) else (
    echo An error occured
)